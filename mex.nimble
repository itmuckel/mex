# Package

version       = "0.1.0"
author        = "itmuckel"
description   = "mex"
license       = "Proprietary"
srcDir        = "src"
binDir        = "bin"
bin           = @["mex"]



# Dependencies

requires "nim >= 1.0.2"
requires "sdl2_nim 2.0.10"
requires "glm 1.1.1"
requires "opengl 1.2.3"