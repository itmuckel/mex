import assimp/assimp
import glm
import opengl
import os
import math
import util
import components
import texture
import options
import strutils

type
  Vertex* = object
    position*: Vec3f
    normal*: Vec3f
    texCoords*: Vec2f

  Texture* = ref object
    id*: GLuint
    typ*: TTextureType
    path: string

  Mesh* = object
    vertices*: seq[Vertex]
    indices*: seq[GLuint]
    textures*: seq[Texture]
    ambient*: Option[Vec3f]
    diffuse*: Option[Vec3f]
    specular*: Option[Vec3f]
    vao*, vbo*, ebo*: GLuint

  Model* = ref object
    meshes*: seq[Mesh]
    directory*: string
    file*: string
    texturesLoaded: seq[Texture]

var ModelCache*: seq[Model]

proc setup(self: var Mesh, shader: GLuint) =
  glGenVertexArrays(1, addr(self.vao))
  glGenBuffers(1, addr(self.vbo))
  glGenBuffers(1, addr(self.ebo))

  glBindVertexArray(self.vao);
  glBindBuffer(GL_ARRAY_BUFFER, self.vbo)

  glBufferData(GL_ARRAY_BUFFER, self.vertices.len * sizeof(Vertex),
                addr(self.vertices[0]), GL_STATIC_DRAW)

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, self.ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, self.indices.len * sizeof(GLuint), 
                addr(self.indices[0]), GL_STATIC_DRAW)

  # vertex positions
  let position = glGetAttribLocation(shader, "position")
  if position > -1:
    glEnableVertexAttribArray(position)
    glVertexAttribPointer(position, 3, cGL_FLOAT, GL_FALSE, sizeof(Vertex), cast[pointer](0))
  # vertex normals
  let normal = glGetAttribLocation(shader, "normal")
  if normal > -1:
    glEnableVertexAttribArray(normal)
    glVertexAttribPointer(normal, 3, cGL_FLOAT, GL_FALSE, sizeof(Vertex), cast[pointer](offsetof(Vertex, normal)))
  # vertex texture coords
  let texCoord = glGetAttribLocation(shader, "texCoord")
  if texCoord > -1:
    glEnableVertexAttribArray(texCoord)
    glVertexAttribPointer(texCoord, 2, cGL_FLOAT, GL_FALSE, sizeof(Vertex), cast[pointer](offsetof(Vertex, texCoords)))

  glBindVertexArray(0)

proc draw(self: var Mesh, shader: GLuint, position: Vec2[GL_FLOAT],
                                      scale: GLfloat = 1f,
                                      rotation: Rotation = 0.toRotation) =
  if self.ambient.isSome:
    let ambientUni = glGetUniformLocation(shader, "objAmbient")
    glUniform3fv(ambientUni, 1, self.ambient.get.caddr)
  if self.diffuse.isSome:
    let diffuseUni = glGetUniformLocation(shader, "objDiffuse")
    glUniform3fv(diffuseUni, 1, self.diffuse.get.caddr)
  if self.specular.isSome:
    let specularUni = glGetUniformLocation(shader, "objSpecular")
    glUniform3fv(specularUni, 1, self.specular.get.caddr)

  if self.textures.len != 0:
    glBindTexture(GL_TEXTURE_2D, self.textures[0].id)
    glUniform1i(glGetUniformLocation(shader, "readFromTexture"), 1)
  else:
    glUniform1i(glGetUniformLocation(shader, "readFromTexture"), 0)

  let rotation = float32(rotation)
  var model = mat4[float32](1f)
                      .translate(vec3f(position, 0f))
                      .rotateZ(rotation.degToRad)
                      .scale(vec3f(scale))

  let uniModel = glGetUniformLocation(shader, "model")
  glUniformMatrix4fv(uniModel, 1, GL_FALSE, model.caddr)

  glBindVertexArray(self.vao)
  glDrawElements(GL_TRIANGLES, self.indices.len, GL_UNSIGNED_INT, nil)
  glBindVertexArray(0)

proc draw*(self: Model, shader: GLuint, position: Vec2[GL_FLOAT],
                                        scale: GLfloat = 1f,
                                        rotation: Rotation = 0.toRotation) =
  glUseProgram(shader)

  for mesh in self.meshes.mitems:
    mesh.draw(shader, position, scale, rotation)

  glUseProgram(0)

proc getColorFromAssimp(material: PMaterial, key: MatKey): Option[Vec3f] =
  var color: TColor4d
  if getMaterialColor(material, key, addr(color)):
    return some(vec3f(color.r, color.g, color.b))
  else:
    return none(Vec3f)

proc readTextureFromMaterial(self: var Model, material: PMaterial, typ: TTextureType): Texture =
  let numTextures = getTextureCount(material, typ)

  if numTextures > 0:
    var texturePath: AIstring
    # just grab the first texture and ignore the rest
    if not getTexture(material, TexDiffuse, 0, addr(texturePath)):
      return nil

    let path = ($texturePath).replace("\0", "")

    var texture: Texture
    var textureAlreadyLoaded = false
    for tex in self.texturesLoaded:
      if tex.path == path:
        texture = tex
        textureAlreadyLoaded = true
        break

    if not textureAlreadyLoaded:
      texture = Texture()
      texture.typ = TexDiffuse
      texture.id = loadTexture(self.directory / path).id
      texture.path = path
      self.texturesLoaded.add(texture)

    return texture

  return nil

proc processMesh(self: var Model, mesh: PMesh, scene: PScene, shader: GLuint): Mesh =
  # process vertices
  for i in 0..<mesh.vertexCount:
    var vertex: Vertex = Vertex()
    var vec = Vec3f()

    vec.x = mesh.vertices[i].x
    vec.y = mesh.vertices[i].y
    vec.z = mesh.vertices[i].z
    vertex.position = vec

    vec.x = mesh.normals[i].x
    vec.y = mesh.normals[i].y
    vec.z = mesh.normals[i].z
    vertex.normal = vec

    if mesh.texCoords[0] != nil:
      var vec = Vec2f()
      vec.x = mesh.texCoords[0][i].x
      vec.y = mesh.texCoords[0][i].y
      vertex.texCoords = vec
    else:
      vertex.texCoords = Vec2f()

    result.vertices.add(vertex)

  # process indices
  for i in 0..<mesh.faceCount:
    let face = mesh.faces[i]

    for j in 0..<face.indexCount:
      result.indices.add(face.indices[j])

  # process material
  if mesh.materialIndex >= 0:
    let material = scene.materials[mesh.materialIndex]

    result.diffuse = getColorFromAssimp(material, AI_MATKEY_COLOR_DIFFUSE)
    result.ambient = getColorFromAssimp(material, AI_MATKEY_COLOR_AMBIENT)
    result.specular = getColorFromAssimp(material, AI_MATKEY_COLOR_SPECULAR)

    var tex = self.readTextureFromMaterial(material, TexDiffuse)
    if tex != nil:
      result.textures.add(tex)

    tex = self.readTextureFromMaterial(material, TexNormals)
    if tex != nil:
      result.textures.add(tex)

  result.setup(shader)

proc processNode(self: var Model, node: PNode, scene: PScene, shader: GLuint) =
  # process all the node's meshes (if any)
  for i in 0..<node.meshCount:
    let aiMesh = scene.meshes[node.meshes[i]]
    self.meshes.add(self.processMesh(aiMesh, scene, shader))

  # then do the same for each of it's children
  for i in 0..<node.childrenCount:
    self.processNode(node.children[i], scene, shader)
  
proc loadModel*(path: string, shader: GLuint): Model =
  aiEnableVerboseLogging(true)


  for model in ModelCache:
    if cmpPaths(model.directory / model.file, path) == 0:
      return model

  let scene = assimp.aiImportFile(path, aiProcess_FlipUVs)

  if scene == nil or scene.rootNode == nil:
    raise newException(IOError, "Error loading model from " & path)

  result = Model()
  result.directory = path.parentDir
  result.file = path.extractFilename

  result.processNode(scene.rootNode, scene, shader)

  ModelCache.add(result)

  aiReleaseImport(scene)
