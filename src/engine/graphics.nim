import glm
import opengl
import sequtils
import sugar
import math

import util
import components
import modelloader
import texture

type
  Sprite* = ref object
    vao*, vbo*, ebo*: GLuint
    model: Mat4f
    zPos*: float32
    shaderProgram*: GLuint
    width, height: int

  SpriteObject* = object
    transform*: Transform
    velocity*: Velocity
    glObject*: Sprite

  ModelObject* = object
    transform*: Transform
    velocity*: Velocity
    glObject*: Model
    # TODO: Write destructor to prevent memory leak.
    # https://nim-lang.org/docs/destructors.html#lifetimeminustracking-hooks-eqdestroy-hook
    # Otherwise the gl objects won't be freed when the Sprite goes out of scope.

proc destroySprite(self: Sprite) =
  echo "deleting " & repr(self)
  try:
    glDeleteVertexArrays(1, addr(self.vao))
    glDeleteBuffers(1, addr(self.vbo))
    glDeleteBuffers(1, addr(self.ebo))
  except:
    # Deleting buffers fails when OpenGL-Context is already deleted.
    # This could be handled better, for example by deleting all game objects,
    # before the OpenGL-Context is shut down.
    echo ""

proc newSprite*(width, height: int; zPos: float32, shaderProgram: GLuint): Sprite =
  new(result, destroySprite)
  result.width = width
  result.height = height
  result.zPos = zPos
  let width = float32(width)
  let height = float32(height)
  result.shaderProgram = shaderProgram
  result.model = mat4f(1)

  var vertices =
    [ # Position            TexCoords
      0f,  0f, zPos,        0.0f, 0.0f, # Top-left
      width,  0f, zPos,     1.0f, 0.0f, # Top-right
      width, height, zPos,  1.0f, 1.0f, # Bottom-right
      0f, height, zPos,     0.0f, 1.0f, # Bottom-left
    ]
  
  # a textured quad for every object
  var elements =
    [
      0, 2, 1,
      2, 0, 3,
    ].map((x) => GLuint(x))

  glGenVertexArrays(1, addr(result.vao))
  glBindVertexArray(result.vao)

  block initBuffers:
    glGenBuffers(1, addr(result.vbo))
    glBindBuffer(GL_ARRAY_BUFFER, result.vbo)
    glBufferData(GL_ARRAY_BUFFER, vertices.len * sizeof(GLfloat), addr(vertices[0]), GL_STATIC_DRAW)
    glBindBuffer(GL_ARRAY_BUFFER, 0)

    glGenBuffers(1, addr(result.ebo))
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, result.ebo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, elements.len * sizeof(GLuint), addr(elements[0]), GL_STATIC_DRAW)
    # Don't unbind the element buffer!

  block connectDataWithShaders:
    var previousProgram: GLint

    glGetIntegerv(GL_CURRENT_PROGRAM, addr(previousProgram))

    glUseProgram(shaderProgram)

    glBindBuffer(GL_ARRAY_BUFFER, result.vbo)

    let posAttrib = glGetAttribLocation(result.shaderProgram, "position")
    if posAttrib != -1:
      let pos = GLuint(posAttrib)
      glEnableVertexAttribArray(pos)
      glVertexAttribPointer(pos, 3, cGL_FLOAT, GL_FALSE, 
                            5 * sizeof(GL_FLOAT), nil)

    let texAttrib = glGetAttribLocation(result.shaderProgram, "texCoord")
    if texAttrib != -1:
      let tex = GLuint(texAttrib)
      glEnableVertexAttribArray(tex)
      glVertexAttribPointer(tex, 2, cGL_FLOAT, GL_FALSE,
                            5 * sizeof(GL_FLOAT),
                            cast[pointer](3 * sizeof(GL_FLOAT)))

    glUseProgram(GLuint(previousProgram))

    glBindBuffer(GL_ARRAY_BUFFER, 0)

proc draw*(self: var Sprite,
           transform: var Mat4f, instanceCount = 0) =
  glUseProgram(self.shaderProgram)

  let modelUni = glGetUniformLocation(self.shaderProgram, "model")
  glUniformMatrix4fv(modelUni, 1, GL_FALSE, transform.caddr)

  glBindVertexArray(self.vao)

  if instanceCount > 0:
    glDrawElementsInstanced(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nil, instanceCount)
  else:
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nil)

  glBindVertexArray(0)

proc draw*(self: var Sprite,
                   position: Vec2[GL_FLOAT],
                   rotation: Rotation = 0.toRotation) =

  glUseProgram(self.shaderProgram)
  let rotation = float32(rotation)
  self.model = mat4[float32](1f)
                      # position
                      .translate(vec3f(position, 0f))
                      # rotation
                      .translate(vec3f(float32(self.width)/2, float32(self.height)/2, 0f))
                      .rotateZ(rotation.degToRad)
                      .translate(vec3f(-float32(self.width)/2, -float32(self.height)/2, 0f))

  draw(self, self.model)

proc drawImage*(self: var Sprite,
                   image: texture.Texture,
                   position: Vec2[GL_FLOAT],
                   rotation: Rotation = 0.toRotation) =
  glBindTexture(GL_TEXTURE_2D, image.id)
  draw(self, position, rotation)

proc newSpriteObject*(position: Position, shader: GLuint, size: Vec2[int], zPos = 20f): SpriteObject =
  result.transform = Transform(position: position)
  result.glObject = newSprite(size.x, size.y, zPos, shader)

proc newModelObject*(path: string, position: Position, shader: GLuint, scale: GLfloat): ModelObject =
  result.transform = Transform(position: position, scale: scale)
  result.glObject = loadModel(path, shader)