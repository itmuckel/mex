import opengl
import os

proc checkShaderCompilation(shader: GLuint) =
  var status: GLint = 0
  glGetShaderiv(shader, GL_COMPILE_STATUS, addr(status))
  if status != GLint(GL_TRUE):
    stderr.writeLine("Shadercompilation failed!")

proc getShaderCompilationLog(shader: GLuint) =
  var buffer: cstring = newString(512)
  glGetShaderInfoLog(shader, 512, nil, buffer)
  if buffer != "":
    echo "---------------Compilation Log:-----------------------"
    echo buffer
    echo "------------------------------------------------------"

proc loadShaders*(vertShader: string, fragShaders: openArray[string]): GLuint =
  var vertexShader = glCreateShader(GL_VERTEX_SHADER)
  var shaderPath = "assets/shaders"
  block:
    echo "Compiling vertex shader '", vertShader, "'..."
    let shaderSource = allocCStringArray([readFile(shaderPath / vertShader)])
    glShaderSource(vertexShader, 1, shaderSource, nil)
    deallocCStringArray(shaderSource)
    glCompileShader(vertexShader)
    checkShaderCompilation(vertexShader)
    getShaderCompilationLog(vertexShader)
  var fragmentShaders: seq[GLuint]
  block:
    for fragShader in fragShaders:
      echo "Compiling fragment shader '", fragShader, "'..."
      let shader =  glCreateShader(GL_FRAGMENT_SHADER)
      let shaderSource = allocCStringArray([readFile(shaderPath / fragShader)])
      glShaderSource(shader, 1, shaderSource, nil)
      deallocCStringArray(shaderSource)
      glCompileShader(shader)
      checkShaderCompilation(shader)
      getShaderCompilationLog(shader)
      fragmentShaders.add(shader)

  let shaderProgram = glCreateProgram()
  glAttachShader(shaderProgram, vertexShader);
  for shader in fragmentShaders:
    glAttachShader(shaderProgram, shader);

  glBindFragDataLocation(shaderProgram, 0, "outColor")

  glLinkProgram(shaderProgram)

  glDeleteShader(vertexShader)
  for shader in fragmentShaders:
    glDeleteShader(shader)

  return shaderProgram