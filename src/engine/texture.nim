import opengl
import sdl2/sdl, sdl2/sdl_image
import util

type
  Texture* = ref TextureObj
  TextureObj* = object of RootObj
    id*: GLuint # TextureID
    width*, height*: int # Texture dimensions

proc loadTexture*(file: string): Texture =
  var textureID: GLuint = 0

  when defined(linux):
    let file = file & ".png"
  let surface = sdl_image.load(file)

  if surface  == nil:
    stderr.writeLine(sdl.getError())
    raise newException(IOError, file & " couldn't be loaded!")

  glGenTextures(1, addr(textureID))
  glBindTexture(GL_TEXTURE_2D, textureID)

  let mode = if surface.format.BytesPerPixel == 4: GlRGBA else: GlRGB

  glTexImage2D(GL_TEXTURE_2D, 
               0,
               mode,
               surface.w,
               surface.h,
               0,
               mode,
               GL_UNSIGNED_BYTE,
               surface.pixels)

  glGenerateMipmap(GL_TEXTURE_2D)
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

  glBindTexture(GlTexture2D, 0)

  result = Texture()
  result.id = textureID
  result.width = surface.w
  result.height = surface.h
  sdl.freeSurface(surface)

