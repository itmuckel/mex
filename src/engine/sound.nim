import sdl2/[sdl, sdl_mixer]

type
  SoundChunk* = object
    data: Chunk
    playing: bool
    currentChannel: int
    allowOverlap: bool

proc initAudio*() =
  discard sdl_mixer.init(0)

  if sdl.init(sdl.INIT_AUDIO) != 0:
    sdl.logCritical(sdl.LogCategoryAudio,
                    "Can't init audio! %s",
                    sdl.getError())
    return

  if sdl_mixer.openAudio(frequency = 44100, 
                         DEFAULT_FORMAT, 
                         channels = 2, 
                         chunksize = 1024) != 0:
    sdl.logCritical(sdl.LogCategoryAudio,
                    "Can't open audio device! %s",
                    sdl_mixer.getError())

  discard allocateChannels(16)

proc exitAudio*() =
  sdl_mixer.closeAudio()
  while sdl_mixer.init(0) != 0:
    sdl_mixer.quit()

proc newSoundChunk*(path: string, allowOverlap = false): SoundChunk =
  let sound = loadWAV(path)
  if sound == nil:
    echo "Couldn't load sound: ", sdl_mixer.getError()
  result.data = sound
  result.currentChannel = -1
  result.allowOverlap = allowOverlap

proc playSound*(sound: var SoundChunk, loop = 0, msfadeIn = 0) =
  if playing(sound.currentChannel) == 1 and
     getChunk(sound.currentChannel) == sound.data and
     not sound.allowOverlap:
      return
  
  let currentChannel = fadeInChannel(-1, sound.data, loop, msFadeIn)
  if currentChannel == -1:
    sdl.logCritical(sdl.LogCategoryAudio,
                    "Can't play sound! %s",
                    sdl_mixer.getError())
  else:
    sound.currentChannel = currentChannel

proc stopSound*(sound: var SoundChunk, msFadeOut = 0) =
  discard fadeOutChannel(sound.currentChannel, msFadeOut)