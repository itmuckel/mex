import glm
import opengl

type
  Position* = Vec2[GL_FLOAT]
  Velocity* = Vec2[GL_FLOAT]

  # TODO: Add internal mat4f that saves the transformation matrix on every attribute change, so it doesn't need to be calculated in draw calls.
  Transform* = object
    position*: Position
    rotation*: Rotation
    scale*: GL_FLOAT

  Rotation* = object ##  \
                      ##  Represents a rotation in degrees.
                      ##  Cannot overflow/underflow.
    value: int


# TODO: Test all this!
const rotPrecision = 100
const rotMod = 360 * rotPrecision

proc toRotation*(x: float): Rotation =
  let x = int(rotMod + x * rotPrecision) mod rotMod
  return Rotation(value: x)

proc toRotation*(x: int): Rotation = float32(x).toRotation
proc toRotation*(x: float32): Rotation = float(x).toRotation

converter toInt*(src: Rotation): int =
  int(src.value / 100)

converter toFloat*(src: Rotation): float32 =
  src.value / 100

converter toCdouble*(src: Rotation): cdouble =
  cdouble(toFloat(src))

proc `+`*(a: Rotation, b: Rotation): Rotation =
  result.value = (a.value + b.value) mod rotMod

proc `-`*(a: Rotation, b: Rotation): Rotation =
  result.value = (rotMod + (a.value - b.value)) mod rotMod

proc `+=`*(a: var Rotation, b: Rotation) =
  a = a + b

proc `-=`*(a: var Rotation, b: Rotation) =
  a = a - b
