import sdl2/sdl
import opengl
import glm

import modelloader
import texture
import util
import sound
import graphics

export util
export modelloader
export texture
export graphics

type
  App* = ref AppObj
  AppObj* = object
    window*: sdl.Window     # Window pointer
    glContext*: sdl.GLContext

  Camera* = Vec2[GL_FLOAT]

  WindowConfig* = object
    title*: string
    width*, height*: int

var Cam* = Camera(vec2f(0, 0))


#########
# INPUT #
#########

proc getMousePosition*(): Vec2[int] =
  var mouseX, mouseY: cint
  discard getMouseState(addr(mouseX), addr(mouseY))
  vec2(
    int(mouseX) + int(Cam.x),
    int(mouseY) + int(Cam.y)
  )

###############
# APPLICATION #
###############

# Initialization sequence
proc init*(app: App, config: WindowConfig): bool =

  # Init SDL
  if sdl.init(sdl.INIT_VIDEO) != 0:
    sdl.logCritical(sdl.LogCategoryError,
                    "Can't initialize SDL: %s",
                    sdl.getError())
    return false

  discard sdl.glSetAttribute(sdl.GL_CONTEXT_PROFILE_MASK, sdl.GL_CONTEXT_PROFILE_CORE)
  discard sdl.glSetAttribute(sdl.GL_CONTEXT_MAJOR_VERSION, 4)
  discard sdl.glSetAttribute(sdl.GL_CONTEXT_MINOR_VERSION, 4)

  initAudio()

  # Create window
  app.window = sdl.createWindow(
    config.title,
    WINDOW_POS_UNDEFINED,
    WINDOW_POS_UNDEFINED,
    config.width,
    config.height,
    WINDOW_OPENGL or WINDOW_SHOWN)
  if app.window == nil:
    sdl.logCritical(sdl.LogCategoryError,
                    "Can't create window: %s",
                    sdl.getError())
    return false

  # Create renderer
  app.glContext = sdl.glCreateContext(app.window)
  if app.glContext == nil:
    sdl.logCritical(sdl.LogCategoryError,
                    "Can't create glContext: %s",
                    sdl.getError())
    return false

  loadExtensions()

  if sdl.glSetSwapInterval(1) != 0:
    stderr.writeLine("Couldn't enable vertical synchronisation!")
    stderr.writeLine("Please enable VSync for your graphics card, otherwise")
    stderr.writeLine("your graphics card will be *very* busy.")

  glEnable(GL_DEPTH_TEST)
  glEnable(GL_CULL_FACE)
  glCullFace(GL_BACK)
  glFrontFace(GL_CCW)

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  sdl.logInfo(sdl.LogCategoryApplication, "SDL initialized successfully")
  return true


# Shutdown sequence
proc exit*(app: App) =
  sdl.glDeleteContext(app.glContext)
  app.window.destroyWindow()

  exitAudio()

  sdl.logInfo(sdl.LogCategoryApplication, "SDL shutdown completed")
  sdl.quit()