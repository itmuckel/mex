import opengl
import sdl2/sdl

converter toGLint*(x: int): GLint = GLint(x)
converter toGLuint*(x: cint): GLuint = GLuint(x)
converter toGLint*(x: GLenum): GLint = GLint(x)
converter toGLfloat*(x: float32): GLfloat = GLfloat(x)
converter toGLsizei*(x: cint): GLsizei = GLSizei(x)
converter toGLsizei*(x: int): GLsizei = GLSizei(x)
converter toInt*(x: Scancode): int = ord(x)