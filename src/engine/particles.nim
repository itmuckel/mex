import engine
import components
import random
import opengl
import glm
import math
import options
import util

randomize(5)

# TODO: Move this out of "engine" into flames.nim
type
  Particle = object
    offset: Vec2f
    startTransform: Option[Mat4f]
    velocity: float32
    lifetime: float32
    zPos: float32
    stopped: bool

  ParticleSystem* = object
    sprite: Sprite
    particles: seq[Particle]
    stopped: bool
    allStopped: bool

    # for instanced rendering the positions, lifetimes and
    # model matrices  are extracted into own arrays.
    positions: seq[Vec3f]
    lifetimes: seq[GL_FLOAT]
    modelMatrices: seq[Mat4f]
    positions_vbo: GLuint
    lifetimes_vbo: GLuint
    modelMatrices_vbo: GLuint

const particleSize = 10f

proc resetParticle(particle: var Particle) =
  particle.velocity = rand(0.08f..0.18f)
  # spawn a new particle at random x-value in ship's exhaust pipe area.
  particle.offset = vec2f(rand(-13f..13f), 
                          # y-value compensates for ship's movement speed.
                          rand(0f..13f))
  particle.startTransform = none(Mat4f)

proc newParticle(zPos: float32): Particle =
  result = Particle(lifetime: 1f, zPos: zPos)
  resetParticle(result)


proc newParticleSystem*(particleCount: int, shader: GLuint): ParticleSystem =
  result.sprite =  newSprite(int(particleSize), int(particleSize), -52f, shader)

  result.positions.newSeq(particleCount)
  result.lifetimes.newSeq(particleCount)
  result.modelMatrices.newSeq(particleCount)

  for m in result.modelMatrices.mitems:
    m = mat4f(1)

  glBindVertexArray(result.sprite.vao)
  glGenBuffers(1, addr(result.positions_vbo))
  glGenBuffers(1, addr(result.lifetimes_vbo))
  glGenBuffers(1, addr(result.modelMatrices_vbo))

  glUseProgram(shader)

  glBindBuffer(GL_ARRAY_BUFFER, result.positions_vbo)
  let posAttrib = glGetAttribLocation(shader, "worldPosition")
  if posAttrib != -1:
    let pos = GLuint(posAttrib)
    glEnableVertexAttribArray(pos)
    glVertexAttribPointer(pos, 3, cGL_FLOAT, GL_FALSE, 0, nil)
    glVertexAttribDivisor(pos, 1) # one worldPosition per instance

  glBindBuffer(GL_ARRAY_BUFFER, result.lifetimes_vbo)
  let lifetimesAttrib = glGetAttribLocation(shader, "lifetime")
  if lifetimesAttrib != -1:
    let lt = GLuint(lifetimesAttrib)
    glEnableVertexAttribArray(lt)
    glVertexAttribPointer(lt, 1, cGL_FLOAT, GL_FALSE, 0, nil)
    glVertexAttribDivisor(lt, 1) # one lifetime per instance

  glBindBuffer(GL_ARRAY_BUFFER, result.modelMatrices_vbo)
  let modelMatrixAttrib = glGetAttribLocation(shader, "modelMatrix")
  if modelMatrixAttrib != -1:
    let modelMatrix = GLuint(modelMatrixAttrib)

    for i in 0..<4:
      # One Mat4 consumes four attribute locations...
      glEnableVertexAttribArray(modelMatrix + GLuint(i))
      glVertexAttribPointer(modelMatrix + GLuint(i),
                            4, cGL_FLOAT, GL_FALSE,
                            16 * sizeof(GL_FLOAT), # stride
                            cast[pointer](i * 4 * sizeof(GL_FLOAT)))
      glVertexAttribDivisor(modelMatrix + GLuint(i), 1)

  glBindVertexArray(0)

  # the actual particle properties
  for i in 0..<particleCount:
    # it is *very* important that the particles are sorted from
    # furthest to closest (to camera), otherwise transparency won't
    # work as intended
    var particle = newParticle(-52f - float32(i))
    particle.stopped = false
    result.particles.add(particle)
    result.positions[i] = vec3f(particle.offset, particle.zPos)
    result.lifetimes[i] = particle.lifetime
  
  result.stopped = true

proc updateParticles*(self: var ParticleSystem, shader: GLuint) =
  var allStopped = true
  for i in 0..<self.particles.len:
    self.particles[i].offset.y += 7 * self.particles[i].velocity
    # slowly move the particle to the center
    self.particles[i].offset.x -= sign(self.particles[i].offset.x) * 0.08
    # faster particle -> shorter lifespan
    self.particles[i].lifetime += (self.particles[i].velocity / 6)

    if not self.particles[i].stopped:
      allStopped = false

    if self.particles[i].lifetime > 1.0:
      # particle dies.
      # reuse existing particles for performance reasons
      resetParticle(self.particles[i])
      if self.stopped and not self.particles[i].stopped:
        self.particles[i].stopped = true
      elif not self.particles[i].stopped:
        self.particles[i].lifetime = 0f

    # TODO: Omit particles array altogether?
    self.positions[i] = vec3f(self.particles[i].offset, self.particles[i].zPos)
    self.lifetimes[i] = self.particles[i].lifetime

  self.allStopped = allStopped

proc start*(self: var ParticleSystem) =
  if not self.stopped: return

  self.stopped = false

  for particle in self.particles.mitems:
    if particle.stopped:
      resetParticle(particle)
      particle.stopped = false
      particle.lifetime = rand(0f..1f)
  self.allStopped = false

proc stop*(self: var ParticleSystem) =
  self.stopped = true

proc stopped*(self: var ParticleSystem): bool = self.stopped

proc drawParticles*(self: var ParticleSystem, shader: GLuint, transform: Transform) =
  if self.allStopped: return

  glUseProgram(shader)


  glBindVertexArray(self.sprite.vao)

  glBindBuffer(GL_ARRAY_BUFFER, self.positions_vbo)
  # see http://www.opengl.org/wiki/Buffer_Object_Streaming -> orphaning
  glBufferData(GL_ARRAY_BUFFER, self.particles.len * 3 * sizeof(GL_FLOAT), nil, GL_STREAM_DRAW)
  glBufferSubData(GL_ARRAY_BUFFER, 0, self.particles.len * 3 * sizeof(GL_FLOAT), addr(self.positions[0]))

  glBindBuffer(GL_ARRAY_BUFFER, self.lifetimes_vbo)
  glBufferData(GL_ARRAY_BUFFER, self.particles.len * sizeof(GL_FLOAT), nil, GL_STREAM_DRAW)
  glBufferSubData(GL_ARRAY_BUFFER, 0, self.particles.len * sizeof(GL_FLOAT), addr(self.lifetimes[0]))


  for i in 0..<self.particles.len:
    if self.particles[i].startTransform.isNone:
      var transform = mat4f(1)
                              .translate(vec3f(transform.position, -52))
                              # TODO: particles should keep their rotation
                              # for their lifetime.
                              .rotateZ(transform.rotation.degToRad)
                                              # center particles horizontally
                              .translate(vec3f(-0.5 * particleSize,
                                              # move particles down to the spaceship's exhaust pipe
                                              55,
                                              0))
      self.particles[i].startTransform = some(transform)

    self.modelMatrices[i] = self.particles[i].startTransform.get()

  glBindBuffer(GL_ARRAY_BUFFER, self.modelMatrices_vbo)
  glBufferData(GL_ARRAY_BUFFER, self.particles.len * 16 * sizeof(GL_FLOAT), nil, GL_STREAM_DRAW)
  glBufferSubData(GL_ARRAY_BUFFER, 0, self.particles.len * 16 * sizeof(GL_FLOAT), addr(self.modelMatrices[0]))

  glUseProgram(self.sprite.shaderProgram)

  glDrawElementsInstanced(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nil, self.particles.len)

  glBindVertexArray(0)