import glm
import opengl

import engine/[components, engine]

type
  ShotSystem* = object
    shader: GLuint
    shots: seq[SpriteObject]
    velocities: seq[Vec2f]

const SHOT_SPEED = 15f;

proc newShotSystem*(shader: GLuint): ShotSystem =
  result.shader = shader

proc shoot*(self: var ShotSystem, 
            startTransform: Transform,
            shooterVelocity: Vec2f,
            direction: Vec2f) =
  var shot = newSpriteObject(startTransform.position,
                                 self.shader, 
                                 vec2(20, 20), 
                                 zPos = -10)
  shot.transform = startTransform
  shot.transform.position += direction * 80 - vec2f(10, 10)
  self.shots.add(shot)
  self.velocities.add(shooterVelocity + direction * SHOT_SPEED)

proc update*(self: var ShotSystem) =
  for i in 0..<self.shots.len:
    self.shots[i].transform.position += 
        self.velocities[i]

proc draw*(self: var ShotSystem) =
  for shot in self.shots.mitems:
    shot.glObject.draw(shot.transform.position)