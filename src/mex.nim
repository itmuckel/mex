import sdl2/sdl
import sdl2/sdl_image
import opengl
import glm
import tables, times
import math, sets
import random

import engine/[engine, sound, components, shader, particles]
import shots

randomize(5)


const
  Width = 1920
  Height = 1080
  WindowConfig =
    WindowConfig(
                  title: "mex",
                  width: Width,
                  height: Height,
                )

  FPS = 60
  TimeStepMs = 1000f / FPS

  ShipAcceleration = 1f
  MaxSpeed = 300f

  asteroidCount = 20

var
  stars: SpriteObject
  spaceship: ModelObject
  asteroids: seq[ModelObject]
  particleSystem: ParticleSystem
  shotSystem: ShotSystem

  engineRunningSound: SoundChunk
  shotSound: SoundChunk

var
  keyActions = {
    SCANCODE_S:
      proc() =
          spaceship.velocity = vec2f(0, 0),
  }.toTable

  nextActions = initHashSet[Scancode](2)

proc getShipToMouseDelta(): Vec2f =
  let mousePos = getMousePosition()
  let shipPos = spaceship.transform.position

  let deltaX = float32(mousePos.x) - shipPos.x
  let deltaY = float32(mousePos.y) - shipPos.y

  return vec2(deltaX, deltaY).normalize

proc getRotationTowardsCursor(): float32 =
  # let y point upwards for following calculations
  let delta = getShipToMouseDelta() * vec2f(1, -1)

  if delta.x >= 0 and delta.y >= 0:
    return arcsin(delta.x).radToDeg
  elif delta.x >= 0 and delta.y < 0:
    return 90 + arcsin(-delta.y).radToDeg
  elif delta.x < 0 and delta.y < 0:
    return 180 + arcsin(-delta.x).radToDeg
  elif delta.x < 0 and delta.y >= 0:
    return 270 + arcsin(delta.y).radToDeg

proc processContinuousKeyPresses() =
  let keystate = getKeyboardState(nil)

  if keystate[SCANCODE_SPACE] > 0:
    if particleSystem.stopped():
      # starting engine repetitively quickly should always sound the engine!
      stopSound(engineRunningSound) 
    
    playSound(engineRunningSound, loop = -1)
      
    particleSystem.start()
  else:
    if not particleSystem.stopped():
      particleSystem.stop()
      stopSound(engineRunningSound, 200)

  for (key, action) in keyActions.pairs:
    if keystate[key] > 0:
      nextActions = nextActions + toHashSet([key])

proc processEvents(): bool =
  ## Returns true on app shutdown request, otherwise return false

  result = false
  var e: sdl.Event

  processContinuousKeyPresses()

  while sdl.pollEvent(addr(e)) != 0:

    # Quit requested
    if e.kind == sdl.Quit:
      return true

    elif e.kind == sdl.MOUSEBUTTONDOWN and
         e.button.button == sdl.BUTTON_LEFT:
      shotSystem.shoot(spaceship.transform,
                       spaceship.velocity,
                       getShipToMouseDelta())
      shotSound.playSound()


    # Key pressed
    elif e.kind == sdl.KEYDOWN and
         e.key.keysym.sym == sdl.K_ESCAPE:
      return true

########
# MAIN #
########

var
  app = App(window: nil, glContext: nil)
  done = false # Main loop exit condition

proc getMillis(): int64 =
  int64(epochTime() * 1000f)

var shaders: seq[GLuint]
var imageShader, 
    starShader, 
    defaultShader, 
    particleShader,
    shotShader: GLuint

var view: Mat4[float32]

proc viewMatrixFor(position: Camera): Mat4f =
  result = glm.lookAt(vec3(position.x, position.y, 1),
                      vec3(position.x, position.y, 0),
                      vec3f(0, 1, 0))

proc updateGame() =
  if nextActions.len != 0:
    for action in nextActions:
      keyActions[action]()
    nextActions.clear()

  if not particleSystem.stopped:
    let newVelocity = spaceship.velocity + getShipToMouseDelta() * ShipAcceleration
    if newVelocity.length2 < MaxSpeed:
      spaceship.velocity = newVelocity
    # Enable slight course changes by reducing velocity when accelerating
    spaceship.velocity *= 0.99

  particleSystem.updateParticles(particleShader)

  let velocity = vec2f(round(spaceship.velocity.x), round(spaceship.velocity.y))
  spaceship.transform.position += velocity
  stars.transform.position += velocity
  engine.Cam += velocity

  # update camera
  view = viewMatrixFor(engine.Cam)
  for shader in shaders:
    glUseProgram(shader)
    var uniView = glGetUniformLocation(shader, "view")
    glUniformMatrix4fv(uniView, 1, GL_FALSE, view.caddr)
  glUseProgram(0)

  spaceship.transform.rotation = getRotationTowardsCursor().toRotation

  shotSystem.update()

proc drawGame() =
    glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT)

    stars.glObject.draw(stars.transform.position)
    spaceship.glObject.draw(defaultShader, spaceship.transform.position, spaceship.transform.scale, spaceship.transform.rotation)

    for asteroid in asteroids:
      asteroid.glObject.draw(defaultShader, asteroid.transform.position, asteroid.transform.scale)

    glBlendFunc(GL_SRC_ALPHA, GL_ONE)
    shotSystem.draw()

    particleSystem.drawParticles(particleShader, spaceship.transform)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)


    sdl.glSwapWindow(app.window)

proc executeMainLoop() =
  # See http://gameprogrammingpatterns.com/game-loop.html#play-catch-up

  var previous: int64 = getMillis()
  var lag = 0f

  while not done:
    # measure time
    var elapsed = float32(getMillis() - previous)
    lag += elapsed
    previous = getMillis()

    # process input
    if processEvents():
      done = true
      break

    # update game
    while lag >= TimeStepMs:
      updateGame()
      lag -= TimeStepMs

    drawGame()

proc setupConstantsFor(shader: GLuint) =
  glUseProgram(shader)
  view = lookAt[float32](vec3(0f, 0f, 1f), vec3(0f), vec3(0f, 1f, 0f))
  let viewUni = glGetUniformLocation(shader, "view")
  glUniformMatrix4fv(viewUni, 1, GL_FALSE, view.caddr)

  var ortho = ortho[float32](left = 0f, right = Width,
                             bottom = Height, top = 0, # invert y-axis
                             zNear = 450f, zFar = -350f)
  let uniProj = glGetUniformLocation(shader, "proj")
  glUniformMatrix4fv(uniProj, 1, GL_FALSE, ortho.caddr)

  let resolutionUni = glGetUniformLocation(shader, "resolution")
  glUniform2i(resolutionUni, Width, Height)

proc setupLightingFor(shader: GLuint) =
  glUseProgram(shader)

  var lightColor = vec3f(0.8, 0.8, 1.0)
  var ambientStrength = 0.05f
  var diffuseStrength = 0.2f

  let lightUni = glGetUniformLocation(shader, "lightColor")
  glUniform3fv(lightUni, 1, lightColor.caddr)
  let ambientUni = glGetUniformLocation(shader, "ambientStrength")
  glUniform1f(ambientUni, ambientStrength)
  let diffuseUni = glGetUniformLocation(shader, "diffuseStrength")
  glUniform1f(diffuseUni, diffuseStrength)

proc loadSounds() =
  engineRunningSound = newSoundChunk("assets/sound/engine.wav")
  shotSound = newSoundChunk("assets/sound/shot.wav", allowOverlap = true)

########
# MAIN #
########

if init(app, WindowConfig):
  # Load assets
  loadSounds()

  defaultShader = loadShaders("3d.vert",
                              [
                                "3d.frag",
                                "light.frag",
                              ])
  imageShader = loadShaders("image.vert",
                            [
                              "image.frag",
                            ])

  starShader = loadShaders("stars.vert",
                          [
                            "util.frag",
                            "stars.frag",
                          ])

  particleShader = loadShaders("particle.vert",
                              [
                                "util.frag",
                                "particle.frag",
                              ])

  shotShader = loadShaders("shot.vert",
                          [
                            "util.frag",
                            "shot.frag",
                          ])

  shaders.add(@[defaultShader, 
                imageShader, 
                starShader, 
                particleShader,
                shotShader])

  for shader in shaders:
    setupConstantsFor(shader)

  setupLightingFor(defaultShader)

  let spaceshipPos = vec2f(Width / 2, Height / 2)
  spaceship = newModelObject("assets/models/spaceship.obj", spaceshipPos, defaultShader, 18f)

  const spread = 3000f
  for i in 0..<asteroidCount:

    let position = vec2f(rand(-spread..spread), rand(-spread..spread))
    let asteroid = newModelObject("assets/models/ast1.obj", position, defaultShader, rand(3f..15f))

    asteroids.add(asteroid)

  stars = newSpriteObject(vec2(0f, 0f), starShader, vec2(Width, Height))

  particleSystem = newParticleSystem(300, particleShader)

  shotSystem = newShotSystem(shotShader)

  glClearColor(0.4, 0.4, 0.8, 1)

  executeMainLoop()

exit(app)
