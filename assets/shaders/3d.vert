#version 440 core

in vec3 position;
in vec3 normal;
in vec2 texCoord;

out vec3 Normal;
out vec2 TexCoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

void main()
{
  TexCoord = texCoord;
  // mat3, because translation doesn't matter for the normal
  Normal = mat3(model) * normal;
  gl_Position = proj * view * model * vec4(position, 1.0);
}