#version 440 core

in vec3 position;
in vec2 texCoord;

in vec3 worldPosition;
in float lifetime;
in mat4 modelMatrix;

out vec2 TexCoord;
out float Lifetime;

uniform mat4 view;
uniform mat4 proj;

void main()
{
  TexCoord = texCoord;
  Lifetime = lifetime;

  mat4 offsetTranslation = mat4(1);

  offsetTranslation[3][0] = worldPosition.x;
  offsetTranslation[3][1] = worldPosition.y;
  offsetTranslation[3][2] = worldPosition.z;

  gl_Position = proj * view * modelMatrix * offsetTranslation * vec4(position, 1.0);
}