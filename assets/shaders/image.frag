#version 440 core

in vec2 TexCoord;

out vec4 outColor;

uniform sampler2D tex;

void main()
{
  outColor = texture(tex, TexCoord);

  if (outColor.a == 0.0) {
    discard;
  }
}