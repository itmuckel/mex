#version 440 core

in vec3 position;
in vec2 texCoord;

out vec2 TexCoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

void main()
{
  TexCoord = texCoord;
  gl_Position = proj * view * model * vec4(position, 1.0);
}