#version 440 core

uniform vec3 objDiffuse;
uniform sampler2D tex;
uniform bool readFromTexture;

in vec2 TexCoord;

out vec4 outColor;

vec4 lightFragment(vec4 fragColor);

void main()
{


  vec4 color;
  if (readFromTexture == true) {
    color = texture(tex, TexCoord) + vec4(objDiffuse, 1.0);
  } else {
    color = vec4(objDiffuse, 1.0);
  }

  outColor = lightFragment(color);

  if (outColor.a == 0.0) {
    discard;
  }
}