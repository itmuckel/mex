#version 440 core

float rand(float n);
float rand(vec2 v);
vec3 hsv2rgb(vec3 v);
vec3 rgb2hsv(vec3 v);

in vec3 Position;
in vec2 Camera;

out vec4 outColor;

uniform ivec2 resolution;

float aspectRatio = float(resolution.y) / resolution.x;

// ease function
float exponentialOut(float t, float strength);

vec4 starColor(float depth, float density, vec2 offset) {
  float posX = (float(Position.x + offset.x) / resolution.x) * depth / aspectRatio;
  float posY = (float(Position.y + offset.y) / resolution.y) * depth;
  float posXCentered = trunc(posX) + sign(posX) * 0.5;
  float posYCentered = trunc(posY) + sign(posY) * 0.5;

  vec2 positionToSampleFrom = vec2(int(posX), int(posY));
  float randomValue = rand(positionToSampleFrom);

  // discard if randomValue not high enough
  if (randomValue < density) { return vec4(0, 0, 0, 1); }

  // distance from center for circular stars
  float dist = distance(vec2(posXCentered, posYCentered),
                        vec2(posX, posY));

  // determine color of star
  float randForStar  = rand(vec2(posXCentered, posYCentered));

  // the distance from the star center determines the brightness.
  float brightness = randForStar * 0.02 / smoothstep(0., max(0.5, randForStar), dist);

  // star color as hsv value.
  vec3 starColor = vec3(0.16, 0.4, brightness);


  if (randForStar >= 0.96) {
    starColor.x = 0.58; // set hue to blue
  } else if (randForStar >= 0.92) {
    starColor.x = 0.0; // set hue to red
  } else if (randForStar >= 0.8) {
    // yellow star
  } else {
    // white star
    starColor.y = 0.0; // drop saturation to 0, stpq 'swizzles' texture coordinates.
  }

  return vec4(hsv2rgb(starColor), 1); 
}

void main()
{
  outColor = max(starColor(10, 0.97, 0.5 * Camera),
                   starColor(30, 0.96, 0.75 * Camera));

}