#version 440 core

in vec2 TexCoord;

out vec4 outColor;

float exponentialOut(float t, float strength);

void main()
{
  const vec4 color1 = vec4(0.5, 0.5, 1.0, 0.8);
  const vec4 color2 = vec4(1, 1, 1, 1.0);

  vec2 dist = TexCoord - vec2(0.5);
  float radius = dot(dist, dist);

  if (radius > 0.25) { discard; }

  outColor = mix(color1, color2, smoothstep(0, 1, radius/0.25) * 0.5);
}