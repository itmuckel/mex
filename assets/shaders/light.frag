#version 440 core

uniform vec3 lightColor;
uniform float ambientStrength;
uniform float diffuseStrength;

uniform vec3 objAmbient;
uniform vec3 objSpecular;

in vec3 Normal;

vec3 lightDirection = vec3(1.0, -0.4, -0.6);

vec4 lightFragment(vec4 objDiffuse) {
  // ambient
  vec3 ambient = ambientStrength * lightColor;

  // diffuse
  float angle = dot(normalize(Normal), normalize(lightDirection));
  float diff = max(angle, 0.0);
  vec3 diffuse = diff * lightColor;

  // preserve alpha
  vec4 result = vec4(ambient + diffuse * objDiffuse.rgb, objDiffuse.a);

  return clamp(result, 0.0, 1.0);
}