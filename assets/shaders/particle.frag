#version 440 core

in vec2 TexCoord;
in float Lifetime;

out vec4 outColor;

float exponentialOut(float t, float strength);

void main()
{
  float maxDist = distance(vec2(0.5, 0.5), vec2(1, 1));

  float dist = maxDist - distance(vec2(0.5, 0.5), TexCoord) * 2;
  float intensity = exponentialOut(dist, 4);

  outColor = vec4(1, 0.5 - 0.5 * Lifetime, 0, intensity - Lifetime);
}