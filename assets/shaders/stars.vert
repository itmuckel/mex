#version 440 core

in vec2 position;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

out vec3 Position;
out vec2 Camera;

void main()
{
  gl_Position = proj * view * model * vec4(position, 0.0, 1.0);
  Position = vec3(model * vec4(position, 0.0, 1.0));
  Camera = vec2(view * model * vec4(position, 0.0, 1.0));
}